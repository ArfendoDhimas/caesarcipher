class CaesarCipher{

	constructor(text,key,charset = "ASCII"){
		this.text = text;
		this.key = Number.parseInt(key);
		this.charset = charset;
	}

	encrypt(){
		if (isNaN(this.key)){
			return "";
		}
		let p = this.text.length;
		let cipher_text = "";
		let temp;
		if (this.charset == "ASCII")
		{
			for (var i = 0; i < p ; i++) {
				temp = this.text.charCodeAt(i)
				let temp_key = 0;
				while (temp_key < this.key) 
				{
					if (temp < 9) 
					{
						temp++;
					} 
					else if (temp == 9) 
					{
						temp++;
						temp_key++;
					} 
					else if (temp < 31) 
					{
						temp++;
					} 
					else if (temp < 126) 
					{
						temp++;
						temp_key++;
					} 
					else
					{
						temp++;
					}
					temp = temp % 128; // supaya tetap konsisten dengan ASCII
				}
				cipher_text += String.fromCharCode(temp);
			}
		}
		else if (this.charset == "UTF-8")
		{
			for (var i = 0; i < p ; i++) {
				temp = String.fromCharCode((this.text.charCodeAt(i)+this.key)%256);
				cipher_text += temp;
			}
		}
		return cipher_text;
	}

	decrypt(){
		if(isNaN(this.key)){
			return "";
		}
		let p = this.text.length;
		let plain_text = "";
		let temp;
		if (this.charset == "ASCII") {
			for (var i = 0; i < p; i++) {
				temp = this.text.charCodeAt(i);
				let temp_key = this.key;
				while (0 < temp_key)
				{
					if (temp > 32)
					{
						temp--;
						temp_key--;
					}
					else if (temp > 11)
					{
						temp--;
					}
					else if (temp == 11)
					{
						temp--;
						temp_key--;
					}
					else
					{
						temp--;
					}
					temp = (128 + temp) % 128; // supaya tetap konsisten dengan ASCII
				}
				plain_text += String.fromCharCode(temp);
			}
		}
		else if (this.charset == "UTF-8")
		{
			for (var i = 0; i < p; i++) {
				temp = String.fromCharCode((256+this.text.charCodeAt(i)-this.key)%256);
				plain_text += temp;
			}
		}
		return plain_text;
	}
}